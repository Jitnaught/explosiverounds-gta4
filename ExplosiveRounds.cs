﻿using GTA;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ExplosiveRounds
{
    public class ExplosiveRounds : Script
    {
        const string keysString = "keys", holdKeyString = "toggle_script_hold_key", pressKeyString = "toggle_script_press_key", settingsString = "settings", distanceString = "distance_for_explosive_rounds";
        const Keys defaultHoldKey = Keys.RControlKey, defaultPressKey = Keys.E;
        const float defaultDistance = 500.0f;

        bool explosiveRoundsEnabled = false;
        Keys holdKey, pressKey;
        float distance;

        public ExplosiveRounds()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue(holdKeyString, keysString, defaultHoldKey);
                Settings.SetValue(pressKeyString, keysString, defaultPressKey);
                Settings.SetValue(distanceString, settingsString, defaultDistance);
                Settings.Save();
            }

            holdKey = Settings.GetValueKey(holdKeyString, keysString, defaultHoldKey);
            pressKey = Settings.GetValueKey(pressKeyString, keysString, defaultPressKey);
            distance = Settings.GetValueFloat(distanceString, settingsString, defaultDistance);

            Interval = 50;

            KeyDown += ExplosiveRounds_KeyDown;
        }

        private void ExplosiveRounds_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(Keys.RControlKey) && e.Key == Keys.E)
            {
                if (!Game.isMultiplayer || Game.MultiplayerGameMode == GameMode.FreeMode || Game.MultiplayerGameMode == GameMode.PartyMode)
                {
                    if (explosiveRoundsEnabled)
                    {
                        Tick -= ExplosiveRounds_Tick;
                        explosiveRoundsEnabled = false;
                        GC.Collect();
                    }
                    else
                    {
                        GC.Collect();
                        Tick += ExplosiveRounds_Tick;
                        explosiveRoundsEnabled = true;
                    }

                    Game.DisplayText("Explosive Rounds are now " + (explosiveRoundsEnabled ? "enabled" : "disabled"));
                }
                else if (explosiveRoundsEnabled)
                {
                    Tick -= ExplosiveRounds_Tick;
                    explosiveRoundsEnabled = false;
                    GC.Collect();
                }
            }
        }

        Ped[] peds;
        Weapon currentWeapon;
        private void ExplosiveRounds_Tick(object sender, EventArgs e)
        {
            if (!Game.isMultiplayer || Game.MultiplayerGameMode == GameMode.FreeMode || Game.MultiplayerGameMode == GameMode.PartyMode)
            {
                currentWeapon = Player.Character.Weapons.CurrentType;
                if (currentWeapon != Weapon.Heavy_FlameThrower && currentWeapon != Weapon.Heavy_RocketLauncher && currentWeapon != Weapon.Misc_AnyMelee && currentWeapon != Weapon.Melee_Knife && currentWeapon != Weapon.Melee_BaseballBat && currentWeapon != Weapon.Melee_PoolCue && currentWeapon != Weapon.TLAD_Poolcue && currentWeapon != Weapon.None && currentWeapon != Weapon.TBOGT_ExplosiveShotgun && currentWeapon != Weapon.TBOGT_GrenadeLauncher && currentWeapon != Weapon.TBOGT_StickyBomb && currentWeapon != Weapon.Thrown_Grenade && currentWeapon != Weapon.Thrown_Molotov && currentWeapon != Weapon.TLAD_GrenadeLauncher && currentWeapon != Weapon.TLAD_PipeBomb && currentWeapon != Weapon.Unarmed)
                {
                    peds = World.GetPeds(Player.Character.Position, 400);

                    if (peds != null && peds.Length > 0)
                    {
                        for (uint i = 0; i < peds.Length; i++)
                        {
                            if (peds[i] != null && HasCharBeenDamagedByChar(peds[i], Player.Character) && HasCharBeenDamagedByWeapon(peds[i], currentWeapon))
                            {
                                ClearCharLastWeaponDamage(peds[i]);
                                peds[i].isRagdoll = true;
                                World.AddExplosion(peds[i].GetBonePosition(Bone.Root));
                            }
                        }
                    }
                }
            }
        }


        private bool HasCharBeenDamagedByChar(Ped ped1, Ped ped2)
        {
            return GTA.Native.Function.Call<bool>("HAS_CHAR_BEEN_DAMAGED_BY_CHAR", ped1, ped2);
        }

        private bool HasCharBeenDamagedByWeapon(Ped ped, Weapon weapon)
        {
            return GTA.Native.Function.Call<bool>("HAS_CHAR_BEEN_DAMAGED_BY_WEAPON", ped, (int)weapon);
        }

        private void ClearCharLastWeaponDamage(Ped ped)
        {
            GTA.Native.Function.Call("CLEAR_CHAR_LAST_WEAPON_DAMAGE", ped);
        }
    }
}
