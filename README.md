A mod that gives you explosive rounds! That's pretty much it... XD

How to install:
Install .NET Scripthook.
Copy the ExplosiveRounds.net.dll and ExplosiveRounds.ini to the scripts folder in your GTA IV directory.

Controls:
Right-CTRL + E = Toggle explosive rounds

Changeable in the ini file:
The controls.
The distance that pedestrians will explode at. The default is 500.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
